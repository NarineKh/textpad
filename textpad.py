from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import *
import fileinput
from tkinter.colorchooser import *
from tkinter.filedialog import *
 
#=======================================================================
class FontStlye(object):
    def __init__(self,app):
        self.root = Toplevel()
        self.root.title("Font Style")
        #---------------------------------------------------------------
        label = Label(self.root,text="Select a font style...",width=30)
        label.pack()
        #---------------------------------------------------------------
        styles = (
                "normal",
                "bold",
                "italic",
                "underline")
        #---------------------------------------------------------------
        for style in styles:
            Radiobutton(self.root,text=style,variable=app.style,value=style).pack(anchor=W)
        #---------------------------------------------------------------
        frame = Frame(self.root)
        frame.pack()
        applyButton = Button(frame,text="Apply",command=self.applyfontstyle)
        applyButton.pack(side=LEFT)
        acceptButton = Button(frame,text="Accept",command=self.applyfontstyle_exit)
        acceptButton.pack(side=RIGHT)
        #---------------------------------------------------------------
        self.app = app
        
    def applyfontstyle(self):
        self.app.fontstyle = self.app.style.get()
        self.app.txt.tag_add("style", "sel.first", "sel.last")
        self.app.txt.tag_config("style",font=(self.app.font,self.app.fontsize,self.app.fontstyle))
       
        
    def applyfontstyle_exit(self):
        self.app.fontstyle = self.app.style.get()
        self.app.txt.tag_add("style", "sel.first", "sel.last")
        self.app.txt.tag_config("style",font=(self.app.font,self.app.fontsize,self.app.fontstyle))
        self.root.destroy()

#=======================================================================
class FontSize(object):
    def __init__(self,app):
        self.root = Toplevel()
        self.root.title("Font Size")
        #---------------------------------------------------------------
        label = Label(self.root,text="Select a font size...",width=30)
        label.pack()
        #---------------------------------------------------------------
        
        self.scale = Scale(self.root,from_=10,to=25,orient=HORIZONTAL)
        self.scale.pack()
        self.scale.set(app.fontsize)
        
        #---------------------------------------------------------------
        frame = Frame(self.root)
        frame.pack()
        applyButton = Button(frame,text="Apply",command=self.applyfontsize)
        applyButton.pack(side=LEFT)
        acceptButton = Button(frame,text="Accept",command=self.applyfontsize_exit)
        acceptButton.pack(side=RIGHT)
        #---------------------------------------------------------------
        self.app = app
        
    def applyfontsize(self):
        self.app.fontsize = self.scale.get()
        self.app.txt.tag_add("size", "sel.first", "sel.last")
        self.app.txt.tag_config("size",font=(self.app.font,self.app.fontsize,self.app.fontstyle))
        
    
    def applyfontsize_exit(self):
        self.app.fontsize = self.scale.get()
        self.app.txt.tag_add("size", "sel.first", "sel.last")
        self.app.txt.tag_config("size",font=(self.app.font,self.app.fontsize,self.app.fontstyle))
        self.root.destroy()
#=======================================================================
class Window_editor(object):
    file_name = "Untitled"
    changed = False
    fontsize = 12 #Hold the font size;
    font = "Courier New" #Hold the font;
    fontstyle = "normal" #hold the font style;
    def __init__(self,root):
        #Menu Bar
        self.root = root
        root.title("Untitled: NotePad")
        root.geometry("300x300")
       
        # add a menu: ..................................................
        main_menu = Menu(root)
        root.config(menu=main_menu)
        file_menu = Menu(main_menu)
        main_menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Open", command=self.open_file)
        file_menu.add_command(label="Save", command=self.save_file)
        file_menu.add_command(label="Save as", command=self.save_as_file)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.close_window)

        edit_menu = Menu(main_menu)
        main_menu.add_cascade(label="Edit", menu=edit_menu)
        edit_menu.add_command(label="Cut", command=self.cut)
        edit_menu.add_command(label="Copy", command=self.copy)
        edit_menu.add_command(label="Paste", command=self.paste)
        

        option_menu = Menu(main_menu)
        main_menu.add_cascade(label="Options",menu=option_menu)
        option_menu.add_command(label="Font Size",command=self.changefontsize)
        option_menu.add_command(label="Font Style",command=self.changefontstyle)
        option_menu.add_command(label="Color", command=self.color)
        
     
        help_menu = Menu(main_menu)
        main_menu.add_cascade(label="Help", menu=help_menu)
        help_menu.add_command(label="About...", command=self.about)
        

        
        self.scrollbar = Scrollbar(root)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.txt = Text(root, yscrollcommand=self.scrollbar.set)
        self.txt.pack(side=LEFT, fill=BOTH, expand=YES)
        self.txt.bind("<Button-3>",self.context_menu)
        self.scrollbar.config(command=self.txt.yview)

         #---------------------------------------------------------------
        self.fontvar = StringVar() #hold the variable for radiobutton.
        self.fontvar.set(self.font)
        #---------------------------------------------------------------
        self.style = StringVar() #Hold the style variable for radiobutton
        self.style.set(self.fontstyle)
  
        
        # Set the font, font size and font style: ----------------------
        self.txt.config(font=(self.font,self.fontsize,self.fontstyle))
        root.protocol("WM_DELETE_WINDOW",self.close_window)
    
    # Option Menu ------------------------------------------------------
    def changefontsize(self):
        obj = FontSize(self)
        
        
    def changefontstyle(self):
        obj = FontStlye(self)

    def color(self):
        result = askcolor(title = "Colour")
        try:
            self.txt.tag_add("color", "sel.first", "sel.last")
            self.txt.tag_config("color",foreground = result[1])
        except TclError:
            pass

     #----------File menu commands:--------------------------------------
    
           
    def open_file(self):
        filename = str(askopenfilename(title="Open File",filetypes=[('text file','.*')]))
        if len(filename) > 0:
            self.txt.delete("1.0",END)
            try:
                f = open(filename)
                for line in f:
                    self.txt.insert(END,line)
                f.close()
                self.file_name = filename
                self.root.title(filename[filename.rfind("/")+1:] + ": NotePad")
                self.changed = False
            except IOError:
                tkMessageBox.showwarning("Open file","Cannot open this file...") 
                
    def save_file(self):
        if self.file_name == "Untitled":
            self.save_as_file()
        else:
            f = open(self.file_name,"w")
            txt = self.txt.get("1.0",END)
            f.write(txt)
            f.close()
            self.changed = False   
            self.root.title(self.file_name[self.file_name.rfind("/")+1:] + ": NotePad")
    
    def save_as_file(self):
        filename = str(asksaveasfilename(title="Save as File",defaultextension=".txt"))
        if len(filename) > 0:
            f = open(filename,"w")
            txt = self.txt.get("1.0",END)
            f.write(txt)
            f.close()
            self.file_name = filename
            self.root.title(filename[filename.rfind("/")+1:] + ": NotePad")
            self.changed = False


    def close_window(self):
        """Close window and exit programm"""
        if askyesno("Exit", "Do you want to quit?"):
            self.root.destroy()
            
  
    def context_menu(self,event):
        menu = Menu(self.root)
        menu.add_command(label="Cut",command=self.cut)
        menu.add_command(label="Copy",command=self.copy)
        menu.add_command(label="Paste",command=self.paste)
        menu.post(event.x_root,event.y_root)
    
    
     #----------Edit menu commands:--------------------------------------
    def cut(self):
        try:
            self.copy()
            self.txt.delete("sel.first","sel.last")
            self.changed = True
        except TclError:
            pass
        
    def copy(self):
        try:
            self.txt.clipboard_clear()
            txt = self.txt.get("sel.first","sel.last")
            self.txt.clipboard_append(txt)
            
        except TclError:
            pass
                
    def paste(self):
        try:
            txt = self.txt.selection_get(selection="CLIPBOARD")
            self.txt.insert(INSERT,txt)
            self.changed = True
        except TclError:
            pass

    
    
     #----------Help menu commands:--------------------------------------
 
    def about(self):
        showinfo("Editor Authors", "Developed in 2015 (s)")
#------------------------------------------------------------------ 
root = Tk()

root.minsize(width=300, height=300)
root.maxsize(width=500, height=500)

obj_menu = Window_editor(root)
root.mainloop()